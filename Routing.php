<?php

require_once 'src/controllers/DefaultController.php';
require_once 'src/controllers/SecurityController.php';
require_once 'src/controllers/DietController.php';


class Router {

  public static $routes;

  public static function get($url, $view) {
    self::$routes[$url] = $view;
  }

  public static function post($url, $view) {
    self::$routes[$url] = $view;
  }

  public static function run ($url) {
    $actionRequest = explode("/", $url);

    $action = lcfirst(implode('', array_map(fn($path) => ucfirst($path), $actionRequest )));

    if (!array_key_exists($action, self::$routes)) {
      die(" url!");
    }

    $controller = self::$routes[$action];

    $object = new $controller;
    $action = $action ?: 'index';

    $object->$action();
  }
}