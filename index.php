<?php
session_start();

require 'Routing.php';


define('VIEW_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url( $path, PHP_URL_PATH);

Router::get('index', 'DefaultController');
Router::get('users', 'DefaultController');


Router::get('diet', 'DietController');
Router::get('dietCreate', 'DietController');
Router::get('dietDetails', 'DietController');
Router::get('meals', 'DietController');


Router::post('login', 'SecurityController');
Router::post('register', 'SecurityController');
Router::post('logout', 'SecurityController');

Router::run($path);