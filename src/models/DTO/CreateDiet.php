<?php


class CreateDiet
{
    private int $userId;
    private int $sex;
    private int $age;
    private int $height;
    private int $weight;
    private int $type;
    private int $work;
    private int $calories;

    private \DateTime $createdAt;

    /**
     * CreateDiet constructor.
     * @param int $userId
     * @param int $sex
     * @param int $age
     * @param int $height
     * @param int $weight
     * @param int $type
     * @param int $work
     */
    public function __construct(int $userId, int $sex, int $age, int $height, int $weight, int $type, int $work)
    {
        $this->userId = $userId;
        $this->sex = $sex;
        $this->age = $age;
        $this->height = $height;
        $this->weight = $weight;
        $this->type = $type;
        $this->work = $work;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }


    /**
     * @return int
     */
    public function getSex(): int
    {
        return $this->sex;
    }

    /**
     * @param int $sex
     */
    public function setSex(int $sex): void
    {
        $this->sex = $sex;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge(int $age): void
    {
        $this->age = $age;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height): void
    {
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight(int $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getWork(): int
    {
        return $this->work;
    }

    /**
     * @param int $work
     */
    public function setWork(int $work): void
    {
        $this->work = $work;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getCalories(): int
    {
        return $this->calories;
    }

    /**
     * @param int $calories
     */
    public function setCalories(int $calories): void
    {
        $this->calories = $calories;
    }

}