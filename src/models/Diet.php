<?php


class Diet
{
    private $id;
    private $sex;
    private $age;
    private $height;
    private $weight;
    private $type;
    private $work;
    private $createdAt;
    private $active;
    private $calories;

    public const WORK_LOW = 1;
    public const WORK_NORMAL = 2;
    public const WORK_HARD = 3;

    public const DIET_DOWN = 1;
    public const DIET_STILL = 2;
    public const DIET_UP = 3;

    public const SEX_FEMALE = 0;
    public const SEX_MALE = 1;

    public const WORK_TYPES = [
        self::WORK_LOW => 'Siedząca',
        self::WORK_NORMAL => 'Zrównoważona',
        self::WORK_HARD => 'Fizyczna'
    ];

    public const DIET_TYPES = [
        self::DIET_DOWN => 'Chce Schudnąć',
        self::DIET_STILL => 'Utrzyymać mase',
        self::DIET_UP => 'Chcę przytyć'
    ];

    public const SEX_TYPES = [
        self::SEX_FEMALE => 'Kobieta',
        self::SEX_MALE => 'Mężczyzna',
    ];

    /**
     * Diet constructor.
     * @param $sex
     * @param $age
     * @param $height
     * @param $weight
     * @param $type
     * @param $work
     * @param $createdAt
     * @param $active
     * @param $calories
     */
    public function __construct($id, $sex, $age, $height, $weight, $type, $work, $createdAt, $active, $calories)
    {
        $this->id = $id;
        $this->sex = $sex;
        $this->age = $age;
        $this->height = $height;
        $this->weight = $weight;
        $this->type = $type;
        $this->work = $work;
        $this->createdAt = $createdAt;
        $this->active = $active;
        $this->calories = $calories;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param mixed $sex
     */
    public function setSex($sex): void
    {
        $this->sex = $sex;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height): void
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getWork()
    {
        return $this->work;
    }

    /**
     * @param mixed $work
     */
    public function setWork($work): void
    {
        $this->work = $work;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getCalories()
    {
        return $this->calories;
    }

    /**
     * @param mixed $calories
     */
    public function setCalories($calories): void
    {
        $this->calories = $calories;
    }
}