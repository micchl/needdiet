<?php

require_once __DIR__.'/../repository/MealRepository.php';

class DietGenerator
{
    private $diet;
    private MealRepository $mealRepository;

    private const WEIGHT_MULTIPLIER = 10;
    private const HEIGHT_MULTIPLIER = 6.25;
    private const AGE_MULTIPLIER  = 5;

    private const MICRO_MEAL = 297;
    private const SMALL_MEAL = 371;
    private const NORMAL_MEAL = 519;
    private const BIG_MEAL = 742;

    private array $meals = [
        self::MICRO_MEAL => [],
        self::SMALL_MEAL => [],
        self::NORMAL_MEAL => [],
        self::BIG_MEAL => [],
    ];


    private const availableDiets = [
        '1200' => [ self::MICRO_MEAL, self::MICRO_MEAL, self::MICRO_MEAL, self::MICRO_MEAL ],
        '1500' => [ self::SMALL_MEAL, self::SMALL_MEAL, self::SMALL_MEAL, self::SMALL_MEAL ],
        '1750' => [ self::SMALL_MEAL, self::SMALL_MEAL, self::BIG_MEAL, self::SMALL_MEAL ],
        '2000' => [ self::SMALL_MEAL, self::MICRO_MEAL, self::BIG_MEAL, self::NORMAL_MEAL ],
        '2250' => [ self::NORMAL_MEAL, self::SMALL_MEAL, self::BIG_MEAL, self::NORMAL_MEAL ],
        '2500' => [ self::NORMAL_MEAL, self::BIG_MEAL, self::BIG_MEAL, self::NORMAL_MEAL ],
        '2750' => [ self::NORMAL_MEAL, self::BIG_MEAL, self::BIG_MEAL, self::BIG_MEAL ],
        '3000' => [ self::BIG_MEAL, self::BIG_MEAL, self::BIG_MEAL, self::BIG_MEAL ],
    ];

    public function __construct($diet)
    {
        $this->diet = $diet;
        $this->mealRepository = new MealRepository();
    }

    public function generateMealsForDiet()
    {
        $dietOption = $this->matchMealsSetToDiet();
        $dietSet = self::availableDiets[$dietOption];
        $this->loadMeals();

        $startDay = new \DateTime();
        $dietDaysInThisWeek = 7 - (int)$startDay->format('N');
        $meals = [];

        while($dietDaysInThisWeek > 0) {
            $startDay->modify('+1 day');
            $copyOfMeals = $this->meals;

            foreach ($dietSet as $key => $dietMeal) {
                shuffle($copyOfMeals[$dietMeal]);
                $meals[] = [
                    'day' => $startDay->format('Y-m-d'),
                    'type' => $key,
                    'meal_id' => $copyOfMeals[$dietMeal][0]['id']
                ];
                unset($copyOfMeals[$dietMeal][0]);
                shuffle($copyOfMeals[$dietMeal]);
            }


            $dietDaysInThisWeek--;
        }

        return $meals;
    }


    public function caloricDemand() {
        return
            (self::WEIGHT_MULTIPLIER * $this->diet->getWeight()
            + self::HEIGHT_MULTIPLIER * $this->diet->getWeight()
            - self::AGE_MULTIPLIER * $this->diet->getAge()
            + $this->alignmentDiet()
            ) * $this->workMultiplier()
            + $this->goalCalories();
    }

    private function alignmentDiet()
    {
        switch ($this->diet->getSex()) {
            case Diet::SEX_MALE:
                return 5;
                break;
            case Diet::SEX_FEMALE:
                return -161;
                break;
            default:
                return 0;
        }
    }

    private function workMultiplier(): float
    {
        switch ($this->diet->getWork()) {
            case Diet::WORK_LOW:
                return 1.5;
                break;
            case Diet::WORK_NORMAL:
                return 1.8;
                break;
            case Diet::WORK_HARD:
                return 2.2;
                break;
            default:
                return 0;
        }
    }

    private function goalCalories()
    {
        switch ($this->diet->getType()) {
            case Diet::DIET_DOWN:
                return -200;
                break;
            case Diet::DIET_STILL:
                return 0;
                break;
            case Diet::DIET_UP:
                return 200;
                break;
            default:
                return 0;
        }
    }

    private function loadMeals(): void
    {
        $this->meals[self::MICRO_MEAL] = $this->mealRepository->getMealsIdsByCategory(self::MICRO_MEAL);
        $this->meals[self::SMALL_MEAL] = $this->mealRepository->getMealsIdsByCategory(self::SMALL_MEAL);
        $this->meals[self::NORMAL_MEAL] = $this->mealRepository->getMealsIdsByCategory(self::NORMAL_MEAL);
        $this->meals[self::BIG_MEAL] = $this->mealRepository->getMealsIdsByCategory(self::BIG_MEAL);
    }

    private function matchMealsSetToDiet() : int
    {
        $requiredCalories = $this->caloricDemand();
        $diets = array_keys(self::availableDiets);


        foreach ($diets as $key => $val) {

            if ($requiredCalories <= $val) {
                return $val;
            }

            $next = $diets[$key+1];

            if ($val <= $requiredCalories && $val <= $next) {

                if ($requiredCalories - $val < $next - $requiredCalories) {
                    return $val;
                } else {
                    return $next;
                }

            }
        }

        die("Missing set of diets for needed calories");

    }

}