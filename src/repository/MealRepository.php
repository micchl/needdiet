<?php


class MealRepository extends Repository
{
    public function getMealsIdsByCategory(int $calories): array
    {
        $stmt = $this->database->connect()->prepare("
            SELECT id_meals as id FROM meals WHERE calories = :calories
        ");

        $stmt->bindValue(':calories', $calories);
        $stmt->execute();

        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $results;
    }

}