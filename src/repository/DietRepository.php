<?php



class DietRepository extends Repository
{
    public function save(CreateDiet $diet): void
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO diet (user_id, sex, age, height, weight, type, work, calories, created_at)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
        ');


        $stmt->execute([
            $diet->getUserId(),
            $diet->getSex(),
            $diet->getAge(),
            $diet->getHeight(),
            $diet->getWeight(),
            $diet->getType(),
            $diet->getWork(),
            $diet->getCalories(),
            $diet->getCreatedAt()->format('Y-m-d H:i:s')
        ]);

    }

    public function findDietsByUserId(int $userId): array
    {
        $stmt = $this->database->connect()->prepare("
            SELECT * FROM diet WHERE user_id = :userId ORDER BY id DESC
        ");
        $stmt->bindValue(':userId', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function findDietDaysByUser(int $userId) {

            $today = new DateTime();
            $lastDayOfWeek = $today->modify('Next Sunday');

            $stmt = $this->database->connect()->prepare("
            
            SELECT
                DISTINCT dm.day, d.id as diet_id
            FROM diet_meals dm
                     JOIN diet d on dm.diet_id = d.id
            WHERE d.active = true and d.user_id = :userId AND dm.day <= :endOfWeek
            
        ");

            $stmt->bindValue(':userId', $userId, PDO::PARAM_INT);
            $stmt->bindValue(':endOfWeek',$lastDayOfWeek->format('Y-m-d') );

            $stmt->execute();
            $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $results;
    }


    public function findDietByUserId(int $userId): ?Diet
    {
        $stmt = $this->database->connect()->prepare("
            SELECT * FROM diet WHERE user_id = :userId and active = true ORDER BY id DESC
        ");
        $stmt->bindValue(':userId', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (empty($result)) {
            return null;
        }

        return new Diet(
            $result['id'],
            $result['sex'],
            $result['age'],
            $result['height'],
            $result['weight'],
            $result['type'],
            $result['work'],
            $result['created_at'],
            $result['active'],
            $result['calories']
        );
    }

    public function addMealsToDiet(array $meals, int $dietId)
    {
        $connection = $this->database->connect();

        try {
            $connection->beginTransaction();

            foreach ($meals as $meal) {
                $stmt = $connection->prepare("
                 INSERT INTO diet_meals (diet_id, meal_id, type, day)
                VALUES (?, ?, ?, ?)
                ");

                $stmt->execute([
                    $dietId,
                    $meal['meal_id'],
                    $meal['type'],
                    $meal['day']
                ]);
            }


            $connection->commit();
        } catch (\Exception $exception) {
            $connection->rollBack();
            die('Diet generate failure');
        }
    }

    public function findMeals($date, int $dietId) {

        $stmt = $this->database->connect()->prepare("
            
            SELECT dm.type, dm.day,  m.* FROM diet_meals dm
            JOIN meals m on dm.meal_id = m.id_meals
            WHERE dm.diet_id = :dietId and day = :dietDate
            ORDER BY dm.type ASC
            
        ");

        $stmt->bindValue(':dietId', $dietId);
        $stmt->bindValue(':dietDate', $date);

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

}