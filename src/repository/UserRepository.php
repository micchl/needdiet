<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/DTO/NewUser.php';

class UserRepository extends Repository
{

    public function save(NewUser $AddUser): void
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO users (name, surname, email, password)
            VALUES (?, ?, ?, ?)
        ');

        $stmt->execute([
            $AddUser->getName(),
            $AddUser->getSurname(),
            $AddUser->getEmail(),
            $AddUser->getPassword()
        ]);

    }

    public function findUserByEmailAndPassword(string $email, string $password): ?User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.users WHERE email = :email AND password = :pass
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':pass', $password, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user == false) {
            return null;
        }

        return new User(
            $user['id'],
            $user['name'],
            $user['surname'],
            $user['email'],
            $user['password'],
            $user['is_blocked'],
            $user['role']
        );
    }

    public function findAllUsers()
    {
        $stmt = $this->database->connect()->query('
            SELECT * FROM users
        ');

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}