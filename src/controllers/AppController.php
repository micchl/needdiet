<?php

class AppController {

    private $request;

    private array $view = [];

    public function __construct()
    {
        $this->request =  $_SERVER['REQUEST_METHOD'];
    }

    protected function isGet(): bool
    {
        return $this->request === 'GET';
    }

    protected function isPost(): bool
    {
        return $this->request === 'POST';
    }

    protected function bindValueToView($key, $val) {
        $this->view[$key] = $val;
    }


    protected function redirect(string $path) {
        header("Location: /".$path);
    }


    protected function render(string $template = null, array $variables = [])
    {
        $templatePath = 'public/views/'. $template.'.php';
        $output = 'File not found';
                
        if(file_exists($templatePath)) {

            extract(array_merge($variables, $this->view));
            
            ob_start();
            include $templatePath;
            $output = ob_get_clean();
        }

        print $output;
    }

    protected function authCheck()
    {

        if (isset($_SESSION['user']) === false) {
            return $this->render('index', ['messages' => ['Nie jesteś zalogowany']]);
        }

        $this->bindGlobalVariable();

        return true;
    }

    private function bindGlobalVariable(): void
    {
        $this->bindValueToView(
            'username',
            $_SESSION['user']['name'] . ' '. $_SESSION['user']['surname']
        );

        $this->bindValueToView('isAdmin', $_SESSION['user']['role'] === User::ROLE_ADMIN);
    }
}