<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/UserRepository.php';


class DefaultController extends AppController {

    protected UserRepository $userRepository;

    public function __construct()
    {
        parent::__construct();

        $this->bindValueToView('pageTitle', 'Strona Główna');
        $this->userRepository = new UserRepository();
    }

    public function index()
    {
        $this->render('index');
    }

    public function users()
    {
        $this->bindValueToView('Lista użytkowników', 'Strona Główna');
        $this->authCheck();
        return $this->render('users_list', [
            'users' => $this->userRepository->findAllUsers()
        ]);

    }
}