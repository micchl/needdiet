<?php

require_once __DIR__.'/../repository/DietRepository.php';
require_once __DIR__.'/../models/DTO/CreateDiet.php';
require_once __DIR__.'/../models/Diet.php';
require_once __DIR__.'/../Services/DietGenerator.php';


class DietController extends  AppController
{
    protected DietRepository $repository;
    /**
     * DietController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->authCheck();
        $this->repository = new DietRepository();
    }

    public function diet()
    {
        $userId = $_SESSION['user']['id'];
        $diet = $this->repository->findDietByUserId($userId);
        $dietDays = $this->repository->findDietDaysByUser($userId);

        if ($this->isPost()) {
          $dietGenerator = new DietGenerator($diet);
          $meals = $dietGenerator->generateMealsForDiet();
          $this->repository->addMealsToDiet($meals, $diet->getId());
          $this->redirect('diet');
        }

        return $this->render('diet/index',[
            'diet' => $diet,
            'dietDays' => $dietDays
        ]);
    }


    public function dietCreate()
    {
        if ($this->isPost()) {
            if ((int)$_POST['step'] === 1) {
                $_SESSION['diet'] = [
                    'sex' => (int)$_POST['sex'],
                    'age' => (int)$_POST['age'],
                    'height' => (int)$_POST['height'],
                    'weight' => (int)$_POST['weight']
                ];

                $this->render('diet_interview_2');
            } elseif ((int)$_POST['step'] == 2) {

                $_SESSION['diet']['work'] = (int)$_POST['work'];

                $this->render('diet_interview_3');

            } elseif ((int)$_POST['step'] == 3 ) {

                $newDiet =  new CreateDiet(
                    $_SESSION['user']['id'],
                    $_SESSION['diet']['sex'],
                    $_SESSION['diet']['age'],
                    $_SESSION['diet']['height'],
                    $_SESSION['diet']['weight'],
                    (int)$_POST['diet'],
                    $_SESSION['diet']['work']
                );

                $dietGenerator = new DietGenerator($newDiet);
                $newDiet->setCalories(
                    $dietGenerator->caloricDemand()
                );


                $this->repository->save($newDiet);

                unset($_SESSION['diet']);
                $this->redirect('diet');
            }
        }

        return $this->render('diet_interview');
    }

    public function dietDetails()
    {
        $dietList = $this->repository->findDietsByUserId($_SESSION['user']['id']);

        $data = [
            'dietList' => $dietList,
            'workType' => Diet::WORK_TYPES,
            'dietType' => Diet::DIET_TYPES,
            'sexType' => Diet::SEX_TYPES
        ];

        return $this->render('diet/summary', $data);

    }

    public function meals()
    {
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body, true);

        $meals =$this->repository->findMeals($data['date'], (int)$data['id']);

        $mealsType = [
            'Śniadanie',
            'Drugie śniadanie',
            'Obiad',
            'Kolacja',
        ];

        return $this->render('meals', [
            'meals' => $meals,
            'mealsType' => $mealsType
        ]);
    }
}