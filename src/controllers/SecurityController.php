<?php

require_once 'AppController.php';
require_once __DIR__ .'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

class SecurityController extends AppController {

    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function login()
    {
        $this->bindValueToView('pageTitle', 'Logowanie');

        if (!$this->isPost()) {
            return $this->render('login');
        }

        $email = $_POST['email'];
        $password = md5($_POST['password']);

        $user = $this->userRepository->findUserByEmailAndPassword($email, $password);

        if(!$user) {
            return $this->render('login', ['messages' => ['User not exist!']]);
        }

        if ($user->isBlocked()) {
            return $this->render('login', ['messages' => ['User is blocked!']]);
        }

        $_SESSION['user'] = $user->toArray();

        $this->redirect('diet');
    }

    public function register()
    {

        if($this->isPost()){
            $passhashed = md5($_POST['password']);
            $newUser = new NewUser($_POST['imie'], $_POST['nazwisko'],$_POST['email'], $passhashed);
            $this->userRepository->save($newUser);

            return $this->render('login', ['messages' => ['Twoje konto zostało założone. Możesz się zalogować']]);
        }

        return $this->render('register');
    }

    public function logout()
    {
        unset($_SESSION['user']);
        return $this->render('index');

    }
}