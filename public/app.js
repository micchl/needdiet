function loadMeals( dateDiet, dietId ) {


    var data = {
        date: dateDiet,
        id: dietId
    };

    var request = new Request('/meals', {
        method: 'POST',
        body: JSON.stringify(data)
    });

    var output = document.getElementById("meals");

    fetch(request)
        .then(response => {
            return response.text()
        }).then(function (html) {
        output.innerHTML = html;
    }).catch(error => console.log("Błąd: ", error));

}