<?php require_once 'blocks/header.php' ?>

<div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="login-container " >
            <div class="formularz">

                <div class="messages">
                    <?php
                    if (isset($messages)) {
                        foreach ($messages as $message) {
                            echo $message;
                        }
                    }
                    ?>
                </div>

               <button><a href="login"> Zaloguj się</a> </button>
                <button><a href="register"> Założ konto</a> </button>

            </div>
        </div>
    </div>

<?php require_once 'blocks/footer.php' ?>