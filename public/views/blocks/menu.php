<nav>
    <img src="/public/img/logo.svg" >
    <ul>
        <?php if ($isAdmin): ?>
        <li>
            <button class="button-profil">
                <a href="/users"> Lista użytkowników </a>
            </button>
        </li>
        <?php endif; ?>
        <li>
            <button class="button-profil">
                <a href="/diet/create"> Nowa dieta </a>
            </button>
        </li>
        <li>
            <button class="button-profil">
                <a href="/diet">Moja dieta </a>
            </button>
        </li>
        <li>
            <button class="button-profil">
                <a href="/diet/details"> Moje pomiary </a>
            </button>
        </li>
        <li>
            <button class="button-profil">
                <a href="logout"> Wyloguj się </a>
            </button>
        </li>
    </ul>
</nav>