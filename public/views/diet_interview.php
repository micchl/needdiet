<?php require_once 'blocks/header.php' ?>

    <div class="base-container">

        <?php require_once 'blocks/logo.php' ?>

        <p>
            Podstawowe informacje
            </p>
        <form action="/diet/create" method="post">

            <div class="choose">
                 Kobieta <input type="radio" name="sex" value="0">
                Mężczyzna <input type="radio" name="sex" value="1">
            </div>

            <div class="choose">
                Wiek <input name="age" type="text" class="info" required>
                <div class="ramka"> lat </div>
            </div>
            <div class="choose">
                Wzrost <input name="height" type="text" class="info" required>
                <div class="ramka"> cm </div>
            </div>
            <div class="choose">
                Waga <input name="weight" type="text" class="info" required>
                <div class="ramka"> kg </div>
            </div>

            <input type="hidden" name="step" value="1">

            <button type="submit" class="dalej">Dalej</button>
        </form>
    </div>

<?php require_once 'blocks/footer.php' ?>
