<?php require_once VIEW_PATH . 'blocks/header.php' ?>

<body>
<div class="base-container-profil">

    <?php require_once VIEW_PATH . 'blocks/menu.php' ?>

    <main>

        <?php require_once VIEW_PATH . 'blocks/welcome.php' ?>

        <div class="edycja">
            <p> Pomiary </p>

            <table class="table-colored">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Płeć</th>
                    <th>Wzrost</th>
                    <th>Waga</th>
                    <th>Wiek</th>
                    <th>Rodzaj diety</th>
                    <th>Rodzaj pracy</th>
                    <th>Liczba kalorii</th>
                    <th>Data utworzenia</th>
                </tr>
                </thead>
                <tbody>

                <?php if (count($dietList) === 0): ?>
                    <tr><td colspan="8"> Brak diet </td></tr>
                <?php else: ?>


                    <?php
                        $index = 0;
                        foreach ($dietList as $diet):
                        $index++;
                    ?>
                            <tr>
                                <td <?= $diet['active'] ? 'class="active"' : '' ?>>
                                    <?= $index ?>
                                </td>
                                <td <?= $diet['active'] ? 'class="active"' : '' ?>>
                                    <?= $sexType[$diet['sex']] ?>
                                </td>
                                <td <?= $diet['active'] ? 'class="active"' : '' ?>>
                                    <?= $diet['height'] ?>
                                </td>
                                <td <?= $diet['active'] ? 'class="active"' : '' ?>>
                                    <?= $diet['weight'] ?>
                                </td>
                                <td <?= $diet['active'] ? 'class="active"' : '' ?>>
                                    <?= $diet['age'] ?>
                                </td>
                                <td <?= $diet['active'] ? 'class="active"' : '' ?>>
                                    <?= $dietType[$diet['type']] ?>
                                </td>
                                <td <?= $diet['active'] ? 'class="active"' : '' ?>>
                                    <?= $workType[$diet['work']] ?>
                                </td>
                                <td <?= $diet['active'] ? 'class="active"' : '' ?>>
                                    <?= $diet['calories'] ?>
                                </td>
                                <td <?= $diet['active'] ? 'class="active"' : '' ?>>
                                    <?= $diet['created_at'] ?>
                                </td>
                            </tr>
                    <?php endforeach; ?>
                <?php endif; ?>

                </tbody>
            </table>
            <!-- Codes by Quackit.com -->



        </div>


    </main>


</div>
</body>