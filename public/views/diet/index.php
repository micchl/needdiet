<?php require_once VIEW_PATH . 'blocks/header.php' ?>

<body>
    <div class="base-container-profil">

        <?php require_once VIEW_PATH . 'blocks/menu.php' ?>

        <main>

            <?php require_once VIEW_PATH . 'blocks/welcome.php' ?>

            <div class="edycja">
                <p> Dieta</p>


            <?php if ($diet === null): ?>
                <h3>Brak aktywnej diety, <a href="/diet/create">wygeneruj dięte</a> </h3>
            <?php endif; ?>

            </div>
                <section>

                    <?php if (count($dietDays) == 0 && $diet !== null ): ?>
                        <h3>Brak wygenerowanych posiłków</h3>

                        <form action="/diet" method="post">
                            <input type="hidden" name="generate" value="1">
                            <button type="submit"> WYGENERUJ POSIŁKI </button>
                        </form>

                    <?php endif; ?>


                    <?php  if (count($dietDays) > 0): ?>
                   <div class="tydzien">

                     <?php
                     foreach ($dietDays as $day): ?>
                    <button
                            class="dni dietInfo"
                            onclick="loadMeals(<?= '\''.$day['day'].'\'' ?>, <?= $day['diet_id'] ?>)"
                    >
                        <?= $day['day'] ?>
                    </button>
                   <?php endforeach;?>

       
                
                   </div>
                    <?php endif; ?>

                    <div id="meals">

                    </div>
                </section>

        </main>


    </div>
</body>