<?php require_once 'blocks/header.php' ?>

    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="login-container" >
        <form action="login" method="POST" class="formularz">
        <div class="messages">
                    <?php
                        if(isset($messages)){
                            foreach($messages as $message) {
                                echo $message;
                            }
                        }
                    ?>
                    </div>
            <input name="email" type="text" placeholder="email@email.com" class="login">
            <input name="password" type="password" placeholder="password" class="login">
            <button type="submit">Zaloguj się</button>
           <button> <a href="index"> powrót </a> </button>
        </form>
        </div>
    </div>

<?php require_once 'blocks/footer.php' ?>
