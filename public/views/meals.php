<div class="meals">


    <?php foreach ($meals as $meal): ?>

    <div class="meal">
        <h3><?= $mealsType[$meal['type']] ?></h3>
        <h4><?= $meal['name_melas'] ?></h4>
        <p>Składniki: <?= $meal['ingredients'] ?></p>
        <p>Przygotowanie: <?= $meal['preparing'] ?></p>
        <p>Kalorie: <?= $meal['calories'] ?></p>
        <p>Białko: <?= $meal['protein'] ?></p>
        <p>Węglowodany: <?= $meal['carbohydrates'] ?></p>
        <p>Tłuszcz: <?= $meal['fat'] ?></p>

    </div>

    <?php endforeach; ?>

</div>