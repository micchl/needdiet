<?php require_once VIEW_PATH . 'blocks/header.php' ?>

<body>
<div class="base-container-profil">

    <?php require_once VIEW_PATH . 'blocks/menu.php' ?>

    <main>

        <?php require_once VIEW_PATH . 'blocks/welcome.php' ?>

        <div class="edycja">
            <p> Pomiary </p>

            <table class="table-colored">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Imię</th>
                    <th>Nazwisko</th>
                    <th>email</th>
                    <th>Status</th>
                    <th>Rola</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($users as $user):
                        $index++;
                        ?>
                        <tr>
                            <td <?= $user['is_blocked'] ? 'class="blocked"' : '' ?>>
                                <?= $user['id'] ?>
                            </td>
                            <td <?= $user['is_blocked'] ? 'class="blocked"' : '' ?>>
                                <?= $user['name'] ?>
                            </td>
                            <td <?= $user['is_blocked'] ? 'class="blocked"' : '' ?>>
                                <?= $user['surname'] ?>
                            </td>
                            <td <?= $user['is_blocked'] ? 'class="blocked"' : '' ?>>
                                <?= $user['email']?>
                            </td>
                            <td <?= $user['is_blocked'] ? 'class="blocked"' : '' ?>>
                                <?= $user['is_blocked'] ? 'Zablokowany' : 'Aktywny' ?>
                            </td>
                            <td <?= $user['is_blocked'] ? 'class="blocked"' : '' ?>>
                                <?= $user['role'] == 99 ? 'Admin' : 'Użytkownik' ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>



        </div>


    </main>


</div>
</body>